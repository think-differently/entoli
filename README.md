# Entoli
Entoli is a batch script based emulator of Command Prompt (cmd.exe). It can be used when Command Prompt is disabled, but not if batch scripts can't be run. It is not to be used for anything illegal, malicious, or immoral.

Always report bugs at [here](https://gitlab.com/ChristianSirolli/Entoli/issues).

1. Installation
   1. Download the zip file from GitLab and unzip it.
   2. Open the newly unzipped folder. You will find five (5) files: "entoli.bat", "CHANGELOG", "debug.bat", "LICENSE", and "README.md". The first time you run "entoli.bat", Windows' Smart Screen may prevent you from running it simply because it is a file from the internet; you can click `More Info` and then `Run anyway` to run it. I promise there is nothing malicious in this script. Hold me to that promise. Optionally you can view the properties of the script and unblock it, which will cause the Smart Screen to not prevent you from running it.
   3. The script itself is a standalone script, "entoli.bat", completely independent of any other file. Thus, you can copy and paste it or move it to any location, including (but not limited to) a cloud location, CD, DVD or even a USB drive. If you wish to use the debugging feature (which most users likely won't), "debug.bat" needs to be in the same location as "entoli.bat" or else running `debug` will not work. See section 4 for more information.
2. Standard Use
   1. Open Entoli from the location of the script, where ever it may be that you placed it.
   2. You will see the Think Differently command line splash screen. Wait for 2 seconds or press any key to skip it.
   3. It loads the "factory-default" settings or the customized settings (see the next section for how to customize settings).
   4. Once at the command line, you are able to use any Command Prompt command available on the computer it is being used. For a list of the basic commands (non-exhaustive), type `help` and hit `Enter`.
   5. To leave Entoli, type `exit` and hit `Enter` or click the `X` at the top right corner of the window.
3. Changing the Settings
   1. Right click on the script and click "Edit".
   2. Scroll down to where it says `:SETTINGS`. The following code until it says `:START` are the settings for the script. Please continue with caution, as any erroneous edit can easily break the script.
   3. There are three settings that can be changed: the colors, the starting echo, and the starting CD directory.
   4. To change the colors, go to the line that begins with `@SET "COLORS"`, and change the value to any set of colors. See `color /?` in a command prompt to see the possible array of colors.
   5. To change the starting echo to either ECHO ON or ECHO OFF, go to the line that begins with `@SET "ECHOTOGGLE"` and change the value to either TRUE or FALSE, TRUE being for ECHO ON and FALSE being for ECHO OFF.
   6. To change the starting directory, go to the line that begins with `@SET "CDDIR"` and change the value to whatever directory you wish to begin in.
   7. Be sure to save it when you are done.
4. Debug Mode
   1. Debug mode is used to test bugs in Entoli. You can use it by opening "debug.bat" or by typing `debug` in Entoli.
   2. For most issues, Entoli will not close when it runs into an error that would otherwise cause it to close. This allows you to see what error is thrown, if one is.